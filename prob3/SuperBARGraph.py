import csv 
import matplotlib.pyplot as plt 
import numpy as np 
from matplotlib.font_manager import FontProperties

MY_FILE1="../prob3/data/cattle_all.csv"
MY_FILE2="../prob3/data/chickens_all.csv"
MY_FILE3="../prob3/data/duck_all.csv"
MY_FILE4="../prob3/data/goats_all.csv"
MY_FILE5="../prob3/data/pig_all.csv"
MY_FILE6="../prob3/data/sheep.csv"
MY_FILE7="../prob3/data/turkey_all.csv"

def parse(raw_file,delimiter):
	"""parses a raw CSV file to JSON-like object"""

	#open CSV file
	open_file=open(raw_file)

	#Read the CSV data 
	#csv_data=csv.reader(open_file,delimiter=delimiter)
	csv_data=csv.DictReader(open_file)

	#setup an empty list 
	parsed_data=[]

	# Skip over the first line of the file for the headers
	#fields=csv_data.next()

	# Iterate over each row of the csv file, zip together field -> value
	for row in csv_data:
		parsed_data.append(row)
	
	open_file.close()
	
	return parsed_data


def bar():
	data_cattle=parse(MY_FILE1,",")
	data_chicken=parse(MY_FILE2,",")
	data_duck=parse(MY_FILE3,",")
	data_goat=parse(MY_FILE4,",")
	data_pig=parse(MY_FILE5,",")
	data_sheep=parse(MY_FILE6,",")
	data_turkey=parse(MY_FILE7,",")
	
	
	PopCattle=[]
	PopChicken=[]
	PopDuck=[]
	PopGoat=[]
	PopPig=[]
	PopSheep=[]
	PopTurkey=[]
	
	for row in data_cattle:
		PopCattle.append(float(row["Value"]))	

	for row in data_chicken:
		PopChicken.append(float(row["Value"]))		

	for row in data_duck:
		PopDuck.append(float(row["Value"]))	

	for row in data_goat:
		PopGoat.append(float(row["Value"]))
			
	for row in data_pig:
		PopPig.append(float(row["Value"]))	
			
	for row in data_sheep:
		PopSheep.append(float(row["Value"]))	
			
	for row in data_turkey:
		PopTurkey.append(float(row["Value"]))
											
		
		
	continents_tuple=tuple(["Africa","Asia","Austrailia & New Zealand", "Europe", "North America","South America"])		
	plt.xlabel("Continents")
	plt.xticks(range(len(continents_tuple)),continents_tuple,rotation=90)
	plt.subplots_adjust(bottom=0.5)	

	plt.ylabel("Population")
	
	width=0.1
	#testing=[1,2,3,4,5,6]
	plt.rcParams['figure.figsize']=20, 15
	N=6
	ind=np.arange(N)
	cattle=plt.bar(ind,PopCattle,width,color='r')
	chicken=plt.bar(ind+width,PopChicken,width,color='brown')
	duck=plt.bar(ind+width+width, PopDuck,width,color='blue')
	goat=plt.bar(ind+width*3, PopGoat,width,color='black')
	pig=plt.bar(ind+width*4, PopPig,width,color='pink')
	sheep=plt.bar(ind+width*5, PopSheep,width,color='gray')
	turkey=plt.bar(ind+width*6, PopTurkey,width,color='green')
	fontP = FontProperties()
	fontP.set_size('small')
	plt.legend([cattle,chicken,duck,goat,pig,sheep,turkey],["cattle","chicken","duck","goat","pig","sheep","turkey"],prop=fontP,loc="upper center")

	#print PopGoat
	#print PopSheep
	plt.title("Population comparison")
	plt.savefig("SUPER_Population.png")
	
	plt.clf()

def main():
    bar()
    #print parse(MY_FILE, ",")

if __name__ == "__main__":
    main()			
