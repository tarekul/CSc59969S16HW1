import csv 
import matplotlib.pyplot as plt 
import numpy as np 

MY_FILE="../prob3/data/sheep.csv"

def parse(raw_file,delimiter):
	"""parses a raw CSV file to JSON-like object"""

	#open CSV file
	open_file=open(raw_file)

	#Read the CSV data 
	#csv_data=csv.reader(open_file,delimiter=delimiter)
	csv_data=csv.DictReader(open_file)

	#setup an empty list 
	parsed_data=[]

	# Skip over the first line of the file for the headers
	#fields=csv_data.next()

	# Iterate over each row of the csv file, zip together field -> value
	for row in csv_data:
		parsed_data.append(row)
	
	open_file.close()
	
	return parsed_data


def bar():
	data_file=parse(MY_FILE,",")
	continents=[]
	population=[]
	for row in data_file:
		continents.append(row["Country or Area"])
		population.append(row["Value"])
	continents_tuple=tuple(continents)	

	plt.xlabel("Continents")
	plt.xticks(range(len(continents_tuple)),continents_tuple,rotation=90)
	plt.subplots_adjust(bottom=0.5)	

	plt.ylabel("Population")
	width=1/1.5
	plt.bar(range(len(continents_tuple)),population,width)

	plt.title("Sheep Population")
	plt.savefig("sheep_population.png")
	plt.clf()

def main():
    bar()
    #print parse(MY_FILE, ",")

if __name__ == "__main__":
    main()			
