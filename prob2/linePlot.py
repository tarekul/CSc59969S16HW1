import csv 
import matplotlib.pyplot as plt 
import numpy as np 

MY_FILE="../prob2/data/UNdata_Export_20160220_055956741.csv"

def parse(raw_file,delimiter):
	"""parses a raw CSV file to JSON-like object"""

	#open CSV file
	open_file=open(raw_file)

	#Read the CSV data 
	#csv_data=csv.reader(open_file,delimiter=delimiter)
	csv_data=csv.DictReader(open_file)

	#setup an empty list 
	parsed_data=[]

	# Skip over the first line of the file for the headers
	fields=csv_data.next()

	# Iterate over each row of the csv file, zip together field -> value
	for row in csv_data:
		#parsed_data.append(dict(zip(fields, row)))
		parsed_data.append(row)
	
	open_file.close()
	
	return parsed_data	

def visualize_years():
	data_file=parse(MY_FILE, ",")
	year_list=[]
	sorted_yearList=[]
	plt.figure(figsize=(20,15))
	#POPULATION AFRICA
	population_africa=[]
	for row in data_file:
		if(row["Country or Area"]=="Africa +"):
			year_list.append(int(row["Year"]))
			population_africa.append(row["Value"])
	
	
	sorted_yearList=sorted(year_list)
	#print sorted_yearList
	year_tuple=tuple(sorted_yearList)
	
	Africa=plt.plot(population_africa, label="Africa",color='r',linewidth=2.0)
	
	plt.ylabel("Population of Goats")
	#END POPULATION AFRICA

	#POPULATION North America
	population_NorAmerica=[]
	for row in data_file:
		if(row["Country or Area"]=="Northern America +"):
			population_NorAmerica.append(row["Value"])
	
	NorthAmerica=plt.plot(population_NorAmerica, label="North America",color='g',linewidth=2.0)
	

	
	population_SAmerica=[]
	for row in data_file:
		if(row["Country or Area"]=="South America +"):
			population_SAmerica.append(row["Value"])
	
	SouthAmerica=plt.plot(population_SAmerica, label="South America",color='b',linewidth=2.0)
	
	population_Asia=[]
	for row in data_file:
		if(row["Country or Area"]=="Asia +"):
			population_Asia.append(row["Value"])
	
	Asia=plt.plot(population_Asia, label="Asia",color='y',linewidth=2.0)

	population_Europe=[]
	for row in data_file:
		if(row["Country or Area"]=="Europe +"):
			population_Europe.append(row["Value"])
	
	Europe=plt.plot(population_Europe, label="Europe",color='m',linewidth=2.0)
	
	population_AZ=[]
	for row in data_file:
		if(row["Country or Area"]=="Australia and New Zealand +"):
			population_AZ.append(row["Value"])
	
	AZ=plt.plot(population_AZ, label="AZ",color='c',linewidth=2.0)




	legend = plt.legend(loc='upper right', shadow=True)
	plt.xticks(range(len(year_tuple)),year_tuple,rotation=90)

	plt.xlabel("Years")
	plt.subplots_adjust(bottom=0.4)
	plt.title("Population of Goats in different continents, Source: http://data.un.org/Data.aspx?d=FAO&f=itemCode%3a1016")
	
	
	
	plt.savefig("population.png")
	plt.clf()	
		
		
			
	
		





def main():
    print visualize_years()
    #print parse(MY_FILE, ",")

if __name__ == "__main__":
    main()		